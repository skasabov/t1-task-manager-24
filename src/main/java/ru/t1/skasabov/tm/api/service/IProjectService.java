package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name, @Nullable String description,
            @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    void updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @NotNull String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @NotNull String description
    );

    void changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    void changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

}
