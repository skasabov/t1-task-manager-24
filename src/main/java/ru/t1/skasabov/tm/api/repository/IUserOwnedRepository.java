package ru.t1.skasabov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    Boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Integer getSize(@NotNull String userId);

    @Nullable
    M removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M add(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model);

    void removeAll(@NotNull String userId);

}
