package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name, @Nullable String description,
                @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @NotNull String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @NotNull String description
    );

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

}
