package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-remove-by-id";

    @NotNull private static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
