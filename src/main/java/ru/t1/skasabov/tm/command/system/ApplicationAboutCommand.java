package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull private static final String NAME = "about";

    @NotNull private static final String DESCRIPTION = "Show about program.";

    @NotNull private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Stas Kasabov");
        System.out.println("email: stas@kasabov.ru");
        System.out.println("email: stkasabov@yandex.ru");
    }

}
